﻿using Plugin.Geolocator;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecordApp.Model;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewTravelPage : ContentPage
	{
		public NewTravelPage ()
		{
			InitializeComponent ();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();

            var venues = await Venue.GetVenues(position.Latitude, position.Longitude);
            venueListView.ItemsSource = venues;
        }

        private async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            try
            {
                Post.SaveRecord(experienceEntry.Text, venueListView.SelectedItem as Venue);

                await DisplayAlert("Success", "Experience succesfully inserted", "Ok");
                await Navigation.PushAsync(new HomePage());
                await Navigation.PushAsync(new HomePage());
            }
            catch (NullReferenceException)
            {
                await DisplayAlert("Failure", "Experience failed to be inserted", "Ok");

            }
            catch (Exception)
            {
                await DisplayAlert("Failure", "Experience failed to be inserted", "Ok");
            }
        }
    }
}