﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecordApp.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HistoryPage : ContentPage
	{
		public HistoryPage ()
		{
			InitializeComponent ();
		}

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            /*using (SQLiteConnection connection = new SQLiteConnection(App.DatabaseLocation))
            {0
                connection.CreateTable<Post>();
                List<Post> posts = connection.Table<Post>().ToList();
                postListView.ItemsSource = posts;
            }*/

            var posts = await Post.GetUserPosts();
            postListView.ItemsSource = posts;
        }
    }
}
