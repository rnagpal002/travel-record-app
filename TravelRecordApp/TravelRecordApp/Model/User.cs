﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecordApp.Model
{
    public class User
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public User(string Email, string Password)
        {
            this.Email = Email;
            this.Password = Password;
        }

        public User()
        {

        }

        public static async Task<bool> isAuthenticUser(string email, string password)
        {
            var user = (await App.MobileService.GetTable<User>().Where(u => u.Email == email).ToListAsync()).FirstOrDefault();

            if (user != null && user.Password == password)
            {
                App.user = user;
                return true; 
            }
            else
            {
                return false;
            }
        }
        public static async void RegisterUser(User user)
        {
            await App.MobileService.GetTable<User>().InsertAsync(user);
        }
    }
}
