﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecordApp.Model
{
    public class Post
    {
        [PrimaryKey, AutoIncrement]
        public string Id { get; set; }

        [MaxLength(250)]
        public string Experience { get; set; }

        public string VenueName { get; set; }

        public string CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string Address { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Distance { get; set; }

        public string UserId { get; set; }

        public static async void Insert(Post post)
        {
            await App.MobileService.GetTable<Post>().InsertAsync(post);
        }

        public static async Task<List<Post>> GetUserPosts()
        {
            return await App.MobileService.GetTable<Post>().Where(p => p.UserId == App.user.Id).ToListAsync();       
        }

        public static void SaveRecord(string experienceEntry, Venue selectedVenue)
        {
            var firstCategory = selectedVenue.categories.FirstOrDefault();
            Post post = new Post()
            {
                VenueName = selectedVenue.name,
                CategoryId = firstCategory.id,
                CategoryName = firstCategory.name,
                Address = selectedVenue.location.address,
                Latitude = selectedVenue.location.lat,
                Longitude = selectedVenue.location.lng,
                Distance = selectedVenue.location.distance,
                UserId = App.user.Id
            };

            /*using (SQLiteConnection connection = new SQLiteConnection(App.DatabaseLocation))
            {
                connection.CreateTable<Post>();
                int rows = connection.Insert(post);

                if (rows > 0)
                {
                    DisplayAlert("Success", "Experience succesfully inserted", "Ok");
                    Navigation.PushAsync(new HomePage());
                }
                else
                {
                    DisplayAlert("Failure", "Experience failed to be inserted", "Ok");
                }
            }*/

            Insert(post);
        }

        public static Dictionary<string, int> GetPostCategories(List<Post> postTable)
        {
            var categories = postTable.OrderBy(p => p.CategoryId).Select(p => p.CategoryName).Distinct().ToList();

            Dictionary<string, int> categoriesCount = new Dictionary<string, int>();
            for (int i = 0; i < categories.Count; i++)
            {
                var count = postTable.Where(p => p.CategoryName == categories[i]).ToList().Count;

                categoriesCount.Add(categories[i], count);
            }

            return categoriesCount;
        }
    }
}   
